#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=Cygwin_4.x-Windows
CND_DLIB_EXT=dll
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/1728301206/PCL_Collection.o \
	${OBJECTDIR}/_ext/1728301206/PCL_Exception.o \
	${OBJECTDIR}/_ext/1728301206/PCL_GList.o \
	${OBJECTDIR}/_ext/1728301206/PCL_GTree.o \
	${OBJECTDIR}/_ext/1728301206/PCL_Iterator.o \
	${OBJECTDIR}/_ext/1728301206/PCL_List.o \
	${OBJECTDIR}/_ext/1728301206/PCL_Registry.o \
	${OBJECTDIR}/_ext/1728301206/PCL_Root.o \
	${OBJECTDIR}/_ext/1728301206/PCL_Service.o \
	${OBJECTDIR}/_ext/1728301206/PCL_Set.o \
	${OBJECTDIR}/_ext/1728301206/PCL_StrPool.o \
	${OBJECTDIR}/_ext/1728301206/PCL_String.o \
	${OBJECTDIR}/_ext/1728301206/PCL_Vector.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libDLL.${CND_DLIB_EXT}

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libDLL.${CND_DLIB_EXT}: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libDLL.${CND_DLIB_EXT} ${OBJECTFILES} ${LDLIBSOPTIONS} -shared

${OBJECTDIR}/_ext/1728301206/PCL_Collection.o: ../Source/PCL_Collection.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1728301206
	${RM} $@.d
	$(COMPILE.cc) -g  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1728301206/PCL_Collection.o ../Source/PCL_Collection.cpp

${OBJECTDIR}/_ext/1728301206/PCL_Exception.o: ../Source/PCL_Exception.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1728301206
	${RM} $@.d
	$(COMPILE.cc) -g  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1728301206/PCL_Exception.o ../Source/PCL_Exception.cpp

${OBJECTDIR}/_ext/1728301206/PCL_GList.o: ../Source/PCL_GList.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1728301206
	${RM} $@.d
	$(COMPILE.cc) -g  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1728301206/PCL_GList.o ../Source/PCL_GList.cpp

${OBJECTDIR}/_ext/1728301206/PCL_GTree.o: ../Source/PCL_GTree.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1728301206
	${RM} $@.d
	$(COMPILE.cc) -g  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1728301206/PCL_GTree.o ../Source/PCL_GTree.cpp

${OBJECTDIR}/_ext/1728301206/PCL_Iterator.o: ../Source/PCL_Iterator.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1728301206
	${RM} $@.d
	$(COMPILE.cc) -g  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1728301206/PCL_Iterator.o ../Source/PCL_Iterator.cpp

${OBJECTDIR}/_ext/1728301206/PCL_List.o: ../Source/PCL_List.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1728301206
	${RM} $@.d
	$(COMPILE.cc) -g  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1728301206/PCL_List.o ../Source/PCL_List.cpp

${OBJECTDIR}/_ext/1728301206/PCL_Registry.o: ../Source/PCL_Registry.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1728301206
	${RM} $@.d
	$(COMPILE.cc) -g  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1728301206/PCL_Registry.o ../Source/PCL_Registry.cpp

${OBJECTDIR}/_ext/1728301206/PCL_Root.o: ../Source/PCL_Root.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1728301206
	${RM} $@.d
	$(COMPILE.cc) -g  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1728301206/PCL_Root.o ../Source/PCL_Root.cpp

${OBJECTDIR}/_ext/1728301206/PCL_Service.o: ../Source/PCL_Service.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1728301206
	${RM} $@.d
	$(COMPILE.cc) -g  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1728301206/PCL_Service.o ../Source/PCL_Service.cpp

${OBJECTDIR}/_ext/1728301206/PCL_Set.o: ../Source/PCL_Set.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1728301206
	${RM} $@.d
	$(COMPILE.cc) -g  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1728301206/PCL_Set.o ../Source/PCL_Set.cpp

${OBJECTDIR}/_ext/1728301206/PCL_StrPool.o: ../Source/PCL_StrPool.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1728301206
	${RM} $@.d
	$(COMPILE.cc) -g  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1728301206/PCL_StrPool.o ../Source/PCL_StrPool.cpp

${OBJECTDIR}/_ext/1728301206/PCL_String.o: ../Source/PCL_String.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1728301206
	${RM} $@.d
	$(COMPILE.cc) -g  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1728301206/PCL_String.o ../Source/PCL_String.cpp

${OBJECTDIR}/_ext/1728301206/PCL_Vector.o: ../Source/PCL_Vector.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1728301206
	${RM} $@.d
	$(COMPILE.cc) -g  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/1728301206/PCL_Vector.o ../Source/PCL_Vector.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libDLL.${CND_DLIB_EXT}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
