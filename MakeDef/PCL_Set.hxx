#ifndef __PCL_SET_H__
#define __PCL_SET_H__
#ifndef __PCL_H__
#	error Do not include this file directly. Include "PCL.h" instead
#endif
# 1 "PCL_DSet.cpp"
# 1 "<command-line>"
# 1 "PCL_DSet.cpp"


# 1 "../Include/PCL_License.h" 1
# 4 "PCL_DSet.cpp" 2
# 1 "PCL_Internal.h" 1
# 5 "PCL_DSet.cpp" 2





static const uint VSetClassCode = VIRTUALCODE(false); class VSet { public:
# 1 "./Virtual/PCL_VSet.hxx" 1



public:

 virtual uint Length() const =0;
 virtual void SetLength(uint NewLength) =0;
 virtual void SetAll(bool NewValue) =0;
 virtual void Copy(const VSet &SourceSet) =0;
 virtual void SetValue(uint Member, bool NewValue) =0;
 virtual bool Value(uint Member) const =0;

 virtual void Not(void) =0;
 virtual void And(const VSet &Set) =0;
 virtual void Or(const VSet &Set) =0;
 virtual void Xor(const VSet &Set) =0;
 virtual bool IsEqual(const VSet &Set) const =0;
 virtual bool IsEmpty(void) const =0;



public:
 virtual bool operator [](uint Member) const =0;

 virtual const VSet& operator =(bool NewValue) =0;
 virtual const VSet& operator &=(const VSet &Set) =0;
 virtual const VSet& operator |=(const VSet &Set) =0;
 virtual const VSet& operator ^=(const VSet &Set) =0;
 virtual const VSet& operator +=(uint Member) =0;
 virtual const VSet& operator -=(uint Member) =0;

 virtual bool operator ==(const VSet &Set) const =0;
 virtual bool operator !=(const VSet &Set) const =0;



 virtual uint _Size(void) const =0;
 virtual puchar _Addr(void) const =0;
# 12 "PCL_DSet.cpp" 2
};






# 1 "./Library/PCL_Set.hxx" 1







static const uint LSetClassCode = CLASSCODE(VSetClassCode); class PCL_API LSet : public VSet { public: static const uint ClassCode() {return LSetClassCode;}; public:
 LSet(uint InitialLength = 0, bool InitialValue = false);
 LSet(const LSet &Set);
 virtual ~LSet();
 virtual const VSet& operator =(const LSet &Set);
# 1 "./Virtual/PCL_VSet.hxx" 1



public:

 virtual uint Length() const ;
 virtual void SetLength(uint NewLength) ;
 virtual void SetAll(bool NewValue) ;
 virtual void Copy(const VSet &SourceSet) ;
 virtual void SetValue(uint Member, bool NewValue) ;
 virtual bool Value(uint Member) const ;

 virtual void Not(void) ;
 virtual void And(const VSet &Set) ;
 virtual void Or(const VSet &Set) ;
 virtual void Xor(const VSet &Set) ;
 virtual bool IsEqual(const VSet &Set) const ;
 virtual bool IsEmpty(void) const ;



public:
 virtual bool operator [](uint Member) const ;

 virtual const VSet& operator =(bool NewValue) ;
 virtual const VSet& operator &=(const VSet &Set) ;
 virtual const VSet& operator |=(const VSet &Set) ;
 virtual const VSet& operator ^=(const VSet &Set) ;
 virtual const VSet& operator +=(uint Member) ;
 virtual const VSet& operator -=(uint Member) ;

 virtual bool operator ==(const VSet &Set) const ;
 virtual bool operator !=(const VSet &Set) const ;



 virtual uint _Size(void) const ;
 virtual puchar _Addr(void) const ;
# 14 "./Library/PCL_Set.hxx" 2
private:
 LMemory m_set;
 uint m_length;
 uchar m_tailMask;
};
# 19 "PCL_DSet.cpp" 2
#ifdef PCL_TRACEINCLUDES
#	warning "PCL_Set.hxx included"
#endif
#endif // __PCL_SET_H__
