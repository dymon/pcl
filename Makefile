FINALLY = rm

ROOT = .
I = .
O = .

V = 1.99

SUBDIRS = `ls -d *`

MAKEDEF = ./MakeDef/*.* ./MakeDef/Makefile \
	./MakeDef/Library/*.* \
	./MakeDef/Virtual/*.* \
	./MakeDef/Generic/*.*

SOURCE = ./Source/*.*

INCLUDE = ./Include/*.*

STATIC = ./Static/*.* ./Static/Include/*.*

DYNAMIC = ./DLL/*.*

TOOLS = ./Bin/*

LIST = $(MAKEDEF) $(SOURCE) $(INCLUDE) $(STATIC) $(DYNAMIC) $(TOOLS)

build: $(LIST)
	echo $(SUBDIRS) 
	for dir in $(SUBDIRS); do \
	if [ -e $$dir/Makefile ]; then \
	$(MAKE) -C $$dir; \
	fi \
	done

distr: PCL.V$V.Cygwin.zip

PCL.V$V.Cygwin.zip : $(LIST)
	echo $(LIST)
	zip -q $@ ./Install.txt ./tuner.bat $(LIST)
	$(FINALLY) $@

