#include "PCL.hxx"
int main(int argc, char**argv) {
    LString wstr(LFormat("Hello, world!\nsize of wchar_t is %d\n", sizeof(wchar_t)));
    printf("%s", (cstr)wstr);
    for (uint i = 0; i < argc; i++) {
        LString wstr(argv[i]);
        printf("Argument %d is <%s>\n", i, (cstr)wstr);
    }
}