#ifndef __PCL_LIST_H__
#define __PCL_LIST_H__
#ifndef __PCL_H__
#	error Do not include this file directly. Include "PCL.h" instead
#endif
# 1 "PCL_DList.cpp"
# 1 "<command-line>"
# 1 "PCL_DList.cpp"


# 1 "../Include/PCL_License.h" 1
# 4 "PCL_DList.cpp" 2
# 1 "PCL_Internal.h" 1
# 5 "PCL_DList.cpp" 2







typedef enum PCL_Position { ZNullPosition = 0,
 ZHead,
 ZFirst = ZHead,
 ZTop = ZHead,

 ZTail,
 ZLast = ZTail,
 ZBottom = ZTail,
} ZPosition;

typedef enum PCL_Direction { ZNullDirection = 0,
 ZPrev,
 ZUp = ZPrev,
 ZBackward = ZPrev,

 ZNext,
 ZDown = ZNext,
 ZForward = ZNext,
} ZDirection;
static const uint VIteratorClassCode = VIRTUALCODE(false); class VIterator { public:
# 1 "./Virtual/PCL_VIterator.hxx" 1



public:

 virtual void Reset() =0;
 virtual void SetTo(ZPosition Pos) =0;
 virtual bool SetToIndex(int Index) =0;


 virtual bool Move(ZDirection Dir) =0;


 virtual void GetCurrent(pvoid *ppItem) const =0;

 virtual void Get(ZPosition Pos, pvoid *ppItem) const =0;

 virtual void GetByIndex(int Index, pvoid *ppItem) const =0;


 virtual uint Count() const =0;

 virtual uint ItemSize() const =0;
 virtual uint ItemSizeAt(ZPosition Pos) const =0;
 virtual uint ItemSizeByIndex(int Index) const =0;
 virtual bool IsValid() const =0;
 virtual bool IsAt(ZPosition Pos) const =0;




public:
 virtual pvoid operator ()() const =0;

 virtual pvoid operator [](ZPosition Pos) const =0;

 virtual pvoid operator [](int Index) const =0;
# 33 "PCL_DList.cpp" 2
};







typedef bool (*ZFilter)(pvoid pItem);

typedef bool (*ZSortOrder)(pvoid pItem1, pvoid pItem2);

static const VIterator *ZDefaultIterator = NULL;
static const uint VListClassCode = VIRTUALCODE(true); class VList : public VContainer, public VIterator { public:
# 1 "./Virtual/PCL_VList.hxx" 1




protected:
 virtual void AfterInsert(pvoid pItem, uint ItemSize) =0;
 virtual void BeforeRemove(pvoid pItem, uint ItemSize) =0;



public:

 virtual VIterator& NewIterator(ZFilter pFilter = NULL) =0;
 virtual void DeleteIterator(const VIterator& Iter) =0;

 virtual void Insert(ZPosition Pos, pvoid pItem, uint ItemSize = 0) =0;
 virtual bool Remove(ZPosition Pos) =0;
 virtual void RemoveAll() =0;

 virtual void SwapWith(
  ZPosition Pos,
  const VIterator& Iter = *ZDefaultIterator
 ) =0;
 virtual void Swap(
  const VIterator& Iter1,
  const VIterator& Iter2 = *ZDefaultIterator
 ) =0;
 virtual void SortAsc(ZSortOrder Greater) =0;
# 48 "PCL_DList.cpp" 2
};






# 1 "./Generic/PCL_GIterator.hxx" 1







static const uint GIteratorClassCode = CLASSCODE(VIteratorClassCode); class PCL_API GIterator : public VIterator { public: static const uint ClassCode() {return GIteratorClassCode;}; protected: GIterator(); virtual ~GIterator(); protected:

 VList *m_list;
 uint m_index;
 ZFilter m_pFilter;
 virtual bool _Move(ZDirection Dir);
 virtual pvoid _MoveToIndex(uint Index);
 virtual void _MoveValid(ZDirection Dir);

 GIterator(VList *List);

# 1 "./Virtual/PCL_VIterator.hxx" 1



public:

 virtual void Reset() ;
 virtual void SetTo(ZPosition Pos) ;
 virtual bool SetToIndex(int Index) ;


 virtual bool Move(ZDirection Dir) ;


 virtual void GetCurrent(pvoid *ppItem) const ;

 virtual void Get(ZPosition Pos, pvoid *ppItem) const ;

 virtual void GetByIndex(int Index, pvoid *ppItem) const ;


 virtual uint Count() const ;

 virtual uint ItemSize() const ;
 virtual uint ItemSizeAt(ZPosition Pos) const ;
 virtual uint ItemSizeByIndex(int Index) const ;
 virtual bool IsValid() const ;
 virtual bool IsAt(ZPosition Pos) const ;




public:
 virtual pvoid operator ()() const ;

 virtual pvoid operator [](ZPosition Pos) const ;

 virtual pvoid operator [](int Index) const ;
# 20 "./Generic/PCL_GIterator.hxx" 2


 void _IncIndex();
 void _DecIndex();
 void _SetFilter(ZFilter pFilter);
};
# 56 "PCL_DList.cpp" 2
# 1 "./Generic/PCL_GList.hxx" 1







static const uint GListClassCode = CLASSCODE(VListClassCode); class PCL_API GList : public VList { public: static const uint ClassCode() {return GListClassCode;}; protected: GList(); virtual ~GList(); GList(const pcont pContainer); virtual void Import(const pcont pContainer, bool AutoRelease = true); virtual const pcont Export(void); protected:

 pvoid m_head, m_last;
 uint m_itemSize, m_count, m_iterCount;
 VIterator **m_pIter;
 virtual VIterator * _CreateIterator(ZFilter pFilter);
 virtual void _RemoveIterator(VIterator * pIter);
 virtual uint _FindIterator(const VIterator& Iter);
 virtual void _InternalSwap(pvoid pItem1, pvoid pItem2);

# 1 "./Virtual/PCL_VList.hxx" 1




protected:
 virtual void AfterInsert(pvoid pItem, uint ItemSize) ;
 virtual void BeforeRemove(pvoid pItem, uint ItemSize) ;



public:

 virtual VIterator& NewIterator(ZFilter pFilter = NULL) ;
 virtual void DeleteIterator(const VIterator& Iter) ;

 virtual void Insert(ZPosition Pos, pvoid pItem, uint ItemSize = 0) ;
 virtual bool Remove(ZPosition Pos) ;
 virtual void RemoveAll() ;

 virtual void SwapWith(
  ZPosition Pos,
  const VIterator& Iter = *ZDefaultIterator
 ) ;
 virtual void Swap(
  const VIterator& Iter1,
  const VIterator& Iter2 = *ZDefaultIterator
 ) ;
 virtual void SortAsc(ZSortOrder Greater) ;
# 19 "./Generic/PCL_GList.hxx" 2
# 1 "./Virtual/PCL_VIterator.hxx" 1



public:

 virtual void Reset() ;
 virtual void SetTo(ZPosition Pos) ;
 virtual bool SetToIndex(int Index) ;


 virtual bool Move(ZDirection Dir) ;


 virtual void GetCurrent(pvoid *ppItem) const ;

 virtual void Get(ZPosition Pos, pvoid *ppItem) const ;

 virtual void GetByIndex(int Index, pvoid *ppItem) const ;


 virtual uint Count() const ;

 virtual uint ItemSize() const ;
 virtual uint ItemSizeAt(ZPosition Pos) const ;
 virtual uint ItemSizeByIndex(int Index) const ;
 virtual bool IsValid() const ;
 virtual bool IsAt(ZPosition Pos) const ;




public:
 virtual pvoid operator ()() const ;

 virtual pvoid operator [](ZPosition Pos) const ;

 virtual pvoid operator [](int Index) const ;
# 20 "./Generic/PCL_GList.hxx" 2
};
# 57 "PCL_DList.cpp" 2




# 1 "./Library/PCL_List.hxx" 1







static const uint LListClassCode = CLASSCODE(GListClassCode); class PCL_API LList : public GList { public: static const uint ClassCode() {return LListClassCode;}; public: LList(const pcont pContainer); virtual void Import(const pcont pContainer, bool AutoRelease = true); virtual const pcont Export(void); public:
 LList(uint ItemSize);
 virtual ~LList();


 virtual void Insert(ZPosition Pos, pvoid pItem, uint ItemSize = 0);
 virtual bool Remove(ZPosition Pos);
 virtual void RemoveAll();


protected:
 virtual VIterator * _CreateIterator(ZFilter pFilter);
 virtual void _RemoveIterator(VIterator * pIter);
};

typedef struct PCL_ListHeader {
 PCL_ListHeader * pNext;
 PCL_ListHeader * pPrev;
} ZListHeader, *pZListHeader;
# 62 "PCL_DList.cpp" 2
# 1 "./Library/PCL_Vector.hxx" 1







static const uint LVectorClassCode = CLASSCODE(GListClassCode); class PCL_API LVector : public GList { public: static const uint ClassCode() {return LVectorClassCode;}; public: LVector(const pcont pContainer); virtual void Import(const pcont pContainer, bool AutoRelease = true); virtual const pcont Export(void); public:
 LVector(uint ItemSize);
 virtual ~LVector();


 virtual void Insert(ZPosition Pos, pvoid pItem, uint ItemSize = 0);
 virtual bool Remove(ZPosition Pos);
 virtual void RemoveAll();


 virtual void CollectGarbage();
protected:
 virtual VIterator * _CreateIterator(ZFilter pFilter);
 virtual void _RemoveIterator(VIterator * pIter);
 inline uint BlockCount() {return 16;};
 uint m_allocCount;
};
# 63 "PCL_DList.cpp" 2
# 1 "./Library/PCL_Collection.hxx" 1







typedef struct PCL_CollectionHeader {
 uint itemSize;
 LMemory *pMem;
} ZCollectionHeader, *pZCollectionHeader;

class PCL_API _WCollectionHeader : public LVector {
public:
 _WCollectionHeader();
 ~_WCollectionHeader();
 void AfterInsert(pvoid pItem, uint ItemSize);
 void BeforeRemove(pvoid pItem, uint ItemSize);
 pvoid m_pItem;
 uint m_itemSize;
 LException *m_pDbg;
};

static const uint LCollectionClassCode = CLASSCODE(GListClassCode); class PCL_API LCollection : public GList { public: static const uint ClassCode() {return LCollectionClassCode;}; public: LCollection(const pcont pContainer); virtual void Import(const pcont pContainer, bool AutoRelease = true); virtual const pcont Export(void); public:
 friend class LCollectionIterator;
 LCollection();
 virtual ~LCollection();


 virtual VIterator& NewIterator(ZFilter pFilter);
 virtual void DeleteIterator(const VIterator& Iter);

 virtual void Insert(ZPosition Pos, pvoid pItem, uint ItemSize);
 virtual bool Remove(ZPosition Pos);
 virtual void RemoveAll();

 virtual void SwapWith(
  ZPosition Pos,
  const VIterator& Iter = *ZDefaultIterator
 );
 virtual void Swap(
  const VIterator& Iter1,
  const VIterator& Iter2 = *ZDefaultIterator
 );
 virtual void SortAsc(ZSortOrder Greater);






 virtual void GetCurrent(pvoid *ppItem) const;
 virtual pvoid operator ()() const;
 virtual void Get(ZPosition Pos, pvoid *ppItem) const;
 virtual pvoid operator [](ZPosition Pos) const;
 virtual void GetByIndex(int Index, pvoid *ppItem) const;
 virtual pvoid operator [](int Index) const;

 virtual uint Count() const;
 virtual uint ItemSize() const;


 virtual uint ItemSizeByIndex(int Index) const;
 virtual uint ItemSizeAt(ZPosition Pos) const;

 virtual void SetItemSize(
  uint NewSize,
  const VIterator& Iter = *ZDefaultIterator
 );


protected:
 virtual VIterator * _CreateIterator(ZFilter pFilter = NULL);
 virtual void _RemoveIterator(VIterator * pIter);
private:
 _WCollectionHeader m_cHdr;
};
# 64 "PCL_DList.cpp" 2
# 1 "./Library/PCL_Iterator.hxx" 1







static const uint LListIteratorClassCode = CLASSCODE(GIteratorClassCode); class PCL_API LListIterator : public GIterator { public: static const uint ClassCode() {return LListIteratorClassCode;}; public:
 friend class LList;
protected:
 LListIterator(LList *List);
 virtual void Reset();
 virtual void SetTo(ZPosition Pos);
 virtual bool SetToIndex(int Index );
 virtual void GetCurrent(pvoid *ppItem) const;
 virtual pvoid operator ()() const;
 virtual pvoid operator [](ZPosition Pos) const;
 virtual pvoid operator [](int Index) const;

 pvoid m_current;
 virtual void _MoveValid(ZDirection Dir);
 virtual pvoid _MoveToIndex(uint Index);
};

static const uint LVectorIteratorClassCode = CLASSCODE(GIteratorClassCode); class PCL_API LVectorIterator : public GIterator { public: static const uint ClassCode() {return LVectorIteratorClassCode;}; public:
 friend class LVector;
protected:
 LVectorIterator(LVector *Vector);
 virtual void SetTo(ZPosition Pos);
 virtual void GetCurrent(pvoid *ppItem) const;
 virtual pvoid operator ()() const;

 virtual pvoid _MoveToIndex(uint Index);
};

static const uint LCollectionIteratorClassCode = CLASSCODE(GIteratorClassCode); class PCL_API LCollectionIterator : public GIterator { public: static const uint ClassCode() {return LCollectionIteratorClassCode;}; public:
 friend class LCollection;
protected:
 LCollectionIterator(LCollection *Collection);
 ~LCollectionIterator();
 virtual void Reset();
 virtual void SetTo(ZPosition Pos);
 virtual bool SetToIndex(int Index);
 virtual bool Move(ZDirection Dir);

 virtual void GetCurrent(pvoid *ppItem) const;
 virtual pvoid operator ()() const;
 virtual void Get(ZPosition Pos, pvoid *ppItem) const;
 virtual pvoid operator [](ZPosition Pos) const;
 virtual void GetByIndex(int Index, pvoid *ppItem) const;
 virtual pvoid operator [](int Index) const;

 virtual uint Count() const;
 virtual uint ItemSize() const;
 virtual uint ItemSizeAt(ZPosition Pos) const;
 virtual uint ItemSizeByIndex(int Index) const;
 virtual bool IsValid() const;
 virtual bool IsAt(ZPosition Pos) const;
private:
 VIterator &Iter;
};
# 64 "PCL_DList.cpp" 2
#ifdef PCL_TRACEINCLUDES
#	warning "PCL_List.hxx included"
#endif
#endif // __PCL_LIST_H__
