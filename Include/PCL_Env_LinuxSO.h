#ifndef __PCL_ENV_LINUXSO_H__
#define __PCL_ENV_LINUXSO_H__

#ifndef __PCL_H__
#	error Do not include this file directly. Include "PCL.h" instead
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <dlfcn.h>
#include <unistd.h>
#include <ctype.h>
#include "PCL_WinForLin.h"
#define PCL_API_IMP
#define PCL_API_USE

#define PCL_DESCRIPTION "Linux Shared Object [.so]"

#ifdef PCL_TRACEINCLUDES
#	warning "PCL_Env_LinuxSO included"
#endif
#endif // __PCL_ENV_LINUXSO_H__
