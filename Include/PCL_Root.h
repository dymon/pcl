#ifndef __PCL_ROOT_H__
#define __PCL_ROOT_H__
#ifndef __PCL_H__
#	error Do not include this file directly. Include "PCL.h" instead
#endif
# 1 "PCL_DRoot.cpp"
# 1 "<command-line>"
# 1 "PCL_DRoot.cpp"


# 1 "../Include/PCL_License.h" 1
# 4 "PCL_DRoot.cpp" 2
# 1 "PCL_Internal.h" 1
# 5 "PCL_DRoot.cpp" 2
# 1 "PCL_ClassCode.h" 1
# 11 "PCL_ClassCode.h"
static const uint VIRTUALCODE(bool IsContainer) {
 static uint Number = 0;
 if (!Number++) return PCL_PREFIX | PCL_CONTAINER_MASK;
 return PCL_PREFIX | (IsContainer ? PCL_CONTAINER_MASK : 0) | (Number++ << 7);
}
static const uint CLASSCODE(uint BaseCode) {
 static uint Number = 0;
 if (++Number == 256) Number = 1;
 return (BaseCode & 0xFFFFFF00uL) | Number;
}
# 6 "PCL_DRoot.cpp" 2






static const uint VContainerClassCode = VIRTUALCODE(false); class VContainer { public:
# 1 "./Virtual/PCL_VContainer.hxx" 1



public:
 virtual const pcont Export(void) =0;
 virtual void Import(const pcont pContainer, bool AutoRelease = true) =0;
# 14 "PCL_DRoot.cpp" 2
};




static const uint VMemAdmClassCode = VIRTUALCODE(false); class VMemAdm { public:
# 1 "./Virtual/PCL_VMemAdm.hxx" 1



public:
 virtual pvoid Alloc(uint Size) =0;
 virtual pvoid Realloc(pvoid ptr, uint Size) =0;
 virtual void Dealloc(pvoid ptr) =0;
# 21 "PCL_DRoot.cpp" 2
};




typedef enum PCL_MemAdmType { ZNullMemAdmType = 0,
 ZMemDefault = 0,

 ZMemStd = 1,
 ZMemCOM = 2,
 ZMemCustom = 3,
 ZMemUnicode = 4,

 ZMemAdmCount
} ZMemAdmType;




static const uint VMemoryClassCode = VIRTUALCODE(false); class VMemory { public:
# 1 "./Virtual/PCL_VMemory.hxx" 1



public:
 virtual uint Size(void) const =0;
 virtual void SetSize(uint NewSize) =0;
 virtual puchar Addr(void) const =0;
# 42 "PCL_DRoot.cpp" 2
};





static const uint VItemClassCode = VIRTUALCODE(false); class VItem { public:
# 1 "./Virtual/PCL_VItem.hxx" 1



public:
 virtual uint ClassCode(void) const =0;
 virtual void SetClassCode(uint NewClassCode) =0;
 virtual uint Size(void) const =0;
 virtual void SetSize(uint NewSize) =0;
 virtual puchar Addr(void) const =0;
 virtual void SetAddr(puchar NewAddr) =0;
 virtual uint Info(void) const =0;
 virtual void SetInfo(uint NewInfo) =0;



public:
 virtual operator puchar() const =0;
 virtual operator pvoid() const =0;
# 50 "PCL_DRoot.cpp" 2
};




static const uint VCSectClassCode = VIRTUALCODE(false); class VCSect { public:
# 1 "./Virtual/PCL_VCSect.hxx" 1



public:
 virtual void Lock(void) =0;
 virtual void Unlock(void) =0;
 virtual bool TryLock(void) =0;
# 57 "PCL_DRoot.cpp" 2
};




static const uint VRootClassCode = VIRTUALCODE(false); class VRoot { public:
# 1 "./Virtual/PCL_VRoot.hxx" 1
 virtual uint Check(void) =0;
# 64 "PCL_DRoot.cpp" 2
};




static const uint VEventClassCode = VIRTUALCODE(false); class VEvent { public:
# 1 "./Virtual/PCL_VEvent.hxx" 1



public:
 virtual void Signal(void) =0;
 virtual void Clear(void) =0;
 virtual void Pulse(void) =0;
 virtual bool Wait(sint Timeout = INFINITE) =0;
# 71 "PCL_DRoot.cpp" 2
};




static const uint VMutexClassCode = VIRTUALCODE(false); class VMutex { public:
# 1 "./Virtual/PCL_VMutex.hxx" 1



public:
 virtual void Release(void) =0;
 virtual bool Wait(sint Timeout = INFINITE) =0;
# 78 "PCL_DRoot.cpp" 2
};




static const uint VpvoidClassCode = VIRTUALCODE(false);
static const uint VcharClassCode = VIRTUALCODE(false);
static const uint VucharClassCode = VIRTUALCODE(false);
static const uint VshortClassCode = VIRTUALCODE(false);
static const uint VushortClassCode = VIRTUALCODE(false);
static const uint VsshortClassCode = VIRTUALCODE(false);
static const uint VintClassCode = VIRTUALCODE(false);
static const uint VuintClassCode = VIRTUALCODE(false);
static const uint VsintClassCode = VIRTUALCODE(false);
static const uint VlongClassCode = VIRTUALCODE(false);
static const uint VulongClassCode = VIRTUALCODE(false);
static const uint VslongClassCode = VIRTUALCODE(false);
static const uint VfloatClassCode = VIRTUALCODE(false);
static const uint VdoubleClassCode = VIRTUALCODE(false);
static const uint VcstrClassCode = VIRTUALCODE(false);
static const uint VustrClassCode = VIRTUALCODE(false);




typedef enum PCL_RegistrySection { ZNullRegistrySection = 0,
 ZClassInfo,
 ZSystemInfo,
 ZUserInfo,
} ZRegistrySection;

typedef enum PCL_AccessMode { ZNullAccessMode = 0,
 ZNoAccess = 0,
 ZRead = 0x1,
 ZWrite = ZRead << 1,
 ZUpdate = ZRead | ZWrite,
} ZAccessMode;

typedef enum PCL_ControlState { ZNullControlState = 0,
 ZOn = 0x1,
 ZOff = ZOn << 1,
 ZAuto = ZOn | ZOff,
 ZPulse = ZAuto,

 ZInvisible = 0x40000000uL,
 ZDisabled = ZInvisible >> 1,
} ZControlState;

class LException;







# 1 "./Generic/PCL_GMemAdm.hxx" 1







static const uint GMemAdmClassCode = CLASSCODE(VMemAdmClassCode); class PCL_API GMemAdm : public VMemAdm { public: static const uint ClassCode() {return GMemAdmClassCode;}; protected: GMemAdm(); virtual ~GMemAdm(); protected:

 virtual pvoid _Alloc(uint Size);
 virtual pvoid _Realloc(pvoid ptr, uint Size);
 virtual void _Dealloc(pvoid ptr);

# 1 "./Virtual/PCL_VMemAdm.hxx" 1



public:
 virtual pvoid Alloc(uint Size) ;
 virtual pvoid Realloc(pvoid ptr, uint Size) ;
 virtual void Dealloc(pvoid ptr) ;
# 15 "./Generic/PCL_GMemAdm.hxx" 2
};
# 135 "PCL_DRoot.cpp" 2





static const uint LMemAdmClassCode = CLASSCODE(GMemAdmClassCode); class PCL_API LMemAdm : public GMemAdm { public: static const uint ClassCode() {return LMemAdmClassCode;}; public:
protected:
 virtual pvoid _Alloc(uint Size);
 virtual pvoid _Realloc(pvoid ptr, uint Size);
 virtual void _Dealloc(pvoid ptr);
};




static const uint LMemAdmCOMClassCode = CLASSCODE(GMemAdmClassCode); class PCL_API LMemAdmCOM : public GMemAdm { public: static const uint ClassCode() {return LMemAdmCOMClassCode;}; public:
protected:
 virtual pvoid _Alloc(uint Size);
 virtual pvoid _Realloc(pvoid ptr, uint Size);
 virtual void _Dealloc(pvoid ptr);
};





static const uint LMemAdmUnicodeClassCode = CLASSCODE(GMemAdmClassCode); class PCL_API LMemAdmUnicode : public GMemAdm { public: static const uint ClassCode() {return LMemAdmUnicodeClassCode;}; public:
protected:
 virtual pvoid _Alloc(uint Size);
 virtual pvoid _Realloc(pvoid ptr, uint Size);
 virtual void _Dealloc(pvoid ptr);
};




static const uint LMemoryClassCode = CLASSCODE(VMemoryClassCode); class PCL_API LMemory : public VMemory { public: static const uint ClassCode() {return LMemoryClassCode;}; public:
 LMemory(uint IniLength = 0);
 virtual ~LMemory();

# 1 "./Virtual/PCL_VMemory.hxx" 1



public:
 virtual uint Size(void) const ;
 virtual void SetSize(uint NewSize) ;
 virtual puchar Addr(void) const ;
# 176 "PCL_DRoot.cpp" 2

private:
 uint m_len, m_allocLen;
 pvoid m_addr;
};




static const uint LDescrClassCode = CLASSCODE(VItemClassCode);

class PCL_API LDescr : public VItem {
public:
 LDescr(
  puchar Addr = NULL,
  uint Size = 0,
  uint ClassCode = ZUintInvalid,
  uint Info = 0
 );
 LDescr(const LDescr &Copy);
 LDescr(const pZItem pItem);
 LDescr(const uint &Item, uint Info = 0);
 LDescr(const sint &Item, uint Info = 0);
 LDescr(const float &Item, uint Info = 0);
 LDescr(const double &Item, uint Info = 0);
 LDescr(cstr Item, uint Info = 0);
 LDescr(ustr Item, uint Info = 0);

# 1 "./Virtual/PCL_VItem.hxx" 1



public:
 virtual uint ClassCode(void) const ;
 virtual void SetClassCode(uint NewClassCode) ;
 virtual uint Size(void) const ;
 virtual void SetSize(uint NewSize) ;
 virtual puchar Addr(void) const ;
 virtual void SetAddr(puchar NewAddr) ;
 virtual uint Info(void) const ;
 virtual void SetInfo(uint NewInfo) ;



public:
 virtual operator puchar() const ;
 virtual operator pvoid() const ;
# 205 "PCL_DRoot.cpp" 2

private:
 ZItem m_item;
};




static const uint LCSectClassCode = CLASSCODE(VCSectClassCode); class PCL_API LCSect : public VCSect { public: static const uint ClassCode() {return LCSectClassCode;}; public:
 LCSect();

# 1 "./Virtual/PCL_VCSect.hxx" 1



public:
 virtual void Lock(void) ;
 virtual void Unlock(void) ;
 virtual bool TryLock(void) ;
# 217 "PCL_DRoot.cpp" 2

private:
 CRITICAL_SECTION m_cs;
};




static const uint LRootClassCode = CLASSCODE(VRootClassCode); class PCL_API LRoot : public VRoot { public: static const uint ClassCode() {return LRootClassCode;}; public:
 LRoot(uint Reserved);
 ~LRoot();

# 1 "./Virtual/PCL_VRoot.hxx" 1
 virtual uint Check(void) ;
# 230 "PCL_DRoot.cpp" 2

 static const uint VMajor(void) {return PCL_VERMAJOR;};
 static const uint VMinor(void) {return PCL_VERMINOR;};
 static const bool IsDebug(void) {return PCL_ISDEBUG;};
private:
 uint m_initCode;
};







typedef enum PCL_EventReleaseMode {
 ZEventReleaseOne,
 ZEventReleaseAll
} ZEventReleaseMode;

static const uint LEventClassCode = CLASSCODE(VEventClassCode); class PCL_API LEvent : public VEvent { public: static const uint ClassCode() {return LEventClassCode;}; public:
 LEvent(ZEventReleaseMode Type = ZEventReleaseOne);

# 1 "./Virtual/PCL_VEvent.hxx" 1



public:
 virtual void Signal(void) ;
 virtual void Clear(void) ;
 virtual void Pulse(void) ;
 virtual bool Wait(sint Timeout = INFINITE) ;
# 253 "PCL_DRoot.cpp" 2

private:
 ZEventReleaseMode m_type;
 HANDLE m_hEvent;
};




static const uint LMutexClassCode = CLASSCODE(VMutexClassCode); class PCL_API LMutex : public VMutex { public: static const uint ClassCode() {return LMutexClassCode;}; public:
 LMutex(bool InitialyOwned = true);

# 1 "./Virtual/PCL_VMutex.hxx" 1



public:
 virtual void Release(void) ;
 virtual bool Wait(sint Timeout = INFINITE) ;
# 266 "PCL_DRoot.cpp" 2

private:
 HANDLE m_hMutex;
};
#ifdef PCL_TRACEINCLUDES
#	warning "PCL_Root.hxx included"
#endif
#endif // __PCL_ROOT_H__
