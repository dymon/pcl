#ifndef __PCL_TREE_H__
#define __PCL_TREE_H__
#ifndef __PCL_H__
#	error Do not include this file directly. Include "PCL.h" instead
#endif
# 1 "PCL_DTree.cpp"
# 1 "<command-line>"
# 1 "PCL_DTree.cpp"


# 1 "../Include/PCL_License.h" 1
# 4 "PCL_DTree.cpp" 2
# 1 "PCL_Internal.h" 1
# 5 "PCL_DTree.cpp" 2
# 13 "PCL_DTree.cpp"
typedef enum PCL_TreeNodeKind { ZNullTreeNodeKind = 0,
 ZLeaf = 0x1,
 ZSubTree = ZLeaf << 1,
 ZBranch = ZSubTree,
 ZAnyNode = ZLeaf | ZSubTree,
} ZTreeNodeKind;
static const uint VNavigatorClassCode = VIRTUALCODE(false); class VNavigator { public:
# 1 "./Virtual/PCL_VNavigator.hxx" 1



public:


 virtual cstr Path(void) const =0;
 virtual uint Level() const =0;

 virtual uint FindFirst(
  ZTreeNodeKind NodeKind = ZAnyNode,
  cstr NameFilter = NULL
 ) =0;
 virtual uint FindNext(void) =0;

 virtual bool IsLeaf(uint NodeKey) const =0;
 virtual cstr Name(uint NodeKey) const =0;


 virtual VItem &Get(uint NodeKey) const =0;

 virtual void ForgetNode(uint NodeKey) =0;


 virtual bool MoveTo(uint NodeKey) =0;
 virtual bool MoveUp(void) =0;
# 21 "PCL_DTree.cpp" 2
};







static const VNavigator *ZDefaultNavigator = NULL;
static const uint VTreeClassCode = VIRTUALCODE(true); class VTree : public VContainer, public VNavigator { public:
# 1 "./Virtual/PCL_VTree.hxx" 1



protected:
 virtual void AfterInsert(VItem &Item) =0;
 virtual void BeforeRemove(VItem &Item) =0;



public:

 virtual VNavigator& NewNavigator(void) =0;
 virtual void DeleteNavigator(const VNavigator& Nav) =0;


 virtual uint Insert(
  ZTreeNodeKind NodeKind,
  cstr Name,
  VItem &Item



 ) =0;

 virtual bool Copy(cstr NewName, uint NodeKey) =0;
 virtual bool Update(
  uint NodeKey,
  VItem &Item



 ) =0;
 virtual bool Remove(uint NodeKey) =0;

 virtual void RemoveAll(void) =0;
# 32 "PCL_DTree.cpp" 2
};




static const uint VBrowserClassCode = VIRTUALCODE(false); class VBrowser { public:
# 1 "./Virtual/PCL_VBrowser.hxx" 1



public:


 virtual cstr Path(void) const =0;
 virtual uint Level() const =0;

 virtual uint FindFirst(
  ZTreeNodeKind NodeKind = ZAnyNode,
  cstr NameFilter = NULL
 ) =0;
 virtual uint FindNext(void) =0;

 virtual bool IsLeaf(uint NodeKey) const =0;
 virtual cstr Name(uint NodeKey) const =0;


 virtual VItem &Get(uint NodeKey) const =0;

 virtual void ForgetNode(uint NodeKey) =0;


 virtual bool MoveTo(uint NodeKey) =0;
 virtual bool MoveUp(void) =0;
# 39 "PCL_DTree.cpp" 2
};







static const VNavigator *ZDefaultBrowser = NULL;
static const uint VNameSpaceClassCode = VIRTUALCODE(true); class VNameSpace : public VContainer, public VBrowser { public:
# 1 "./Virtual/PCL_VNameSpace.hxx" 1



protected:
 virtual void AfterInsert(VItem &Item) =0;
 virtual void BeforeRemove(VItem &Item) =0;



public:

 virtual VBrowser& NewBrowser(void) =0;
 virtual void DeleteBrowser(const VBrowser& Browser) =0;


 virtual uint Insert(
  ZTreeNodeKind NodeKind,
  cstr Name,
  VItem &Item
 ) =0;
 virtual uint Copy(
  ZTreeNodeKind NodeKind,
  cstr DestinationName,
  uint SourceNodeKey
 ) =0;

 virtual bool Update(
  uint NodeKey,
  VItem &Item
 ) =0;
 virtual bool Remove(uint NodeKey) =0;

 virtual void RemoveAll(void) =0;
# 50 "PCL_DTree.cpp" 2
};
#ifdef PCL_TRACEINCLUDES
#	warning "PCL_Tree.hxx included"
#endif
#endif // __PCL_TREE_H__
