#ifndef __PCL_ENV_CYGWINSO_H__
#define __PCL_ENV_CYGWINSO_H__

#ifndef __PCL_H__
#	error Do not include this file directly. Include "PCL.h" instead
#endif

#include <stdlib.h>
#include <stdio.h>
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#ifndef _WIN32_WINNT
#	define _WIN32_WINNT 0x0400
#endif
#include <windows.h>
#include <objbase.h>
#include <winerror.h>
#define wcslen(X) (strlen((pchar)X) >> 1)
#define PCL_API_IMP __declspec(dllexport)
#define PCL_API_USE __declspec(dllimport)

#define PCL_DESCRIPTION "Cygwin Shared Object [.so]"

#ifdef PCL_TRACEINCLUDES
#	warning "PCL_Env_CygwinSO included"
#endif
#endif // __PCL_ENV_CYGWINSO_H__