#ifndef __PCL_STRING_H__
#define __PCL_STRING_H__
#ifndef __PCL_H__
#	error Do not include this file directly. Include "PCL.h" instead
#endif
# 1 "PCL_DString.cpp"
# 1 "<command-line>"
# 1 "PCL_DString.cpp"


# 1 "../Include/PCL_License.h" 1
# 4 "PCL_DString.cpp" 2
# 1 "PCL_Internal.h" 1
# 5 "PCL_DString.cpp" 2







typedef enum PCL_StrConversionCode { ZNullStrConversionCode = 0,
 ZUpperCase,
 ZLowerCase,
 ZNameStyle,

 ZOEM,
 ZANSI,

} ZStrConversionCode;

inline bool IsEmpty(cstr Str) {
 return !Str || !Str[0];
};

static const uint VStringClassCode = VIRTUALCODE(true); class VString : public VContainer { public:
# 1 "./Virtual/PCL_VString.hxx" 1



public:

 virtual uint Length() const =0;
 virtual void SetBody(cstr Body) =0;
 virtual cstr Body() const =0;
 virtual int Compare(cstr CmpStr, bool IgnoreCase = false) const =0;

 virtual void Append(cstr AppendixExp) =0;
 virtual void Prefix(cstr PrefixExp) =0;
 virtual void Cut(uint Count, ZPosition From = ZHead) =0;
 virtual void Convert(ZStrConversionCode Code) =0;
 virtual void Revert(void) =0;





 virtual char Chr(uint Start) const =0;
 virtual void SetChr(uint Start, char NewChr) =0;
 virtual cstr SubStr(uint Start, uint Count = ZUintMax) =0;
 virtual void Select(uint Start, uint Count = ZUintMax) =0;

 virtual uint FindChr(char SearchChr, ZPosition From = ZHead) =0;
 virtual uint FindStr(cstr SearchStr, ZPosition From = ZHead) =0;
 virtual uint FindAny(cstr SearchStr, ZPosition From = ZHead) =0;
 virtual uint FindNot(cstr SearchStr, ZPosition From = ZHead) =0;

 virtual uint FindNext(void) =0;

 virtual void Replace(cstr ReplExp) =0;


 virtual void ImportUnicode(ustr Src,
  bool AutoRelease = true,
  ZMemAdmType MemAdmCode = ZMemUnicode
 ) =0;
 virtual ustr ExportUnicode(void) =0;

 virtual void ImportEnv(cstr Name) =0;
 virtual void ExportEnv(cstr Name) =0;

 virtual void ImportRes(uint ResID, pvoid Module = NULL) =0;

 virtual void ImportInt(int Src, cstr Format = "%d") =0;
 virtual void ImportHex(uint Src, cstr Format = "%08lx") =0;
 virtual void ImportDbl(double Src, cstr Format = "%lf") =0;



public:
 virtual operator cstr() const =0;
 virtual char& operator [](ZPosition Pos) =0;
 virtual cstr operator ()(uint Start) =0;
 virtual cstr operator ()(uint Start, uint End) =0;

 virtual bool operator ==(cstr Src) const =0;
 virtual bool operator !=(cstr Src) const =0;
 virtual bool operator >=(cstr Src) const =0;
 virtual bool operator <=(cstr Src) const =0;
 virtual bool operator >(cstr Src) const =0;
 virtual bool operator <(cstr Src) const =0;
# 28 "PCL_DString.cpp" 2
};






# 1 "./Library/PCL_String.hxx" 1







static const uint GStringClassCode = CLASSCODE(VStringClassCode); class PCL_API GString : public VString { public: static const uint ClassCode() {return GStringClassCode;}; protected: GString(); virtual ~GString(); GString(const pcont pContainer); virtual void Import(const pcont pContainer, bool AutoRelease = true); virtual const pcont Export(void); protected:
 LMemory m_data, m_clip;
 ustr m_uni;
 struct {
  uint Found;
  uint Len;
  ZPosition Pos;
  uint Mode;
 } m_last;
 virtual uint _FindNext(void);
 virtual uint _FindAny(void);
 virtual uint _FindNot(void);

# 1 "./Virtual/PCL_VString.hxx" 1



public:

 virtual uint Length() const ;
 virtual void SetBody(cstr Body) ;
 virtual cstr Body() const ;
 virtual int Compare(cstr CmpStr, bool IgnoreCase = false) const ;

 virtual void Append(cstr AppendixExp) ;
 virtual void Prefix(cstr PrefixExp) ;
 virtual void Cut(uint Count, ZPosition From = ZHead) ;
 virtual void Convert(ZStrConversionCode Code) ;
 virtual void Revert(void) ;





 virtual char Chr(uint Start) const ;
 virtual void SetChr(uint Start, char NewChr) ;
 virtual cstr SubStr(uint Start, uint Count = ZUintMax) ;
 virtual void Select(uint Start, uint Count = ZUintMax) ;

 virtual uint FindChr(char SearchChr, ZPosition From = ZHead) ;
 virtual uint FindStr(cstr SearchStr, ZPosition From = ZHead) ;
 virtual uint FindAny(cstr SearchStr, ZPosition From = ZHead) ;
 virtual uint FindNot(cstr SearchStr, ZPosition From = ZHead) ;

 virtual uint FindNext(void) ;

 virtual void Replace(cstr ReplExp) ;


 virtual void ImportUnicode(ustr Src,
  bool AutoRelease = true,
  ZMemAdmType MemAdmCode = ZMemUnicode
 ) ;
 virtual ustr ExportUnicode(void) ;

 virtual void ImportEnv(cstr Name) ;
 virtual void ExportEnv(cstr Name) ;

 virtual void ImportRes(uint ResID, pvoid Module = NULL) ;

 virtual void ImportInt(int Src, cstr Format = "%d") ;
 virtual void ImportHex(uint Src, cstr Format = "%08lx") ;
 virtual void ImportDbl(double Src, cstr Format = "%lf") ;



public:
 virtual operator cstr() const ;
 virtual char& operator [](ZPosition Pos) ;
 virtual cstr operator ()(uint Start) ;
 virtual cstr operator ()(uint Start, uint End) ;

 virtual bool operator ==(cstr Src) const ;
 virtual bool operator !=(cstr Src) const ;
 virtual bool operator >=(cstr Src) const ;
 virtual bool operator <=(cstr Src) const ;
 virtual bool operator >(cstr Src) const ;
 virtual bool operator <(cstr Src) const ;
# 22 "./Library/PCL_String.hxx" 2

};

static const uint LStringClassCode = CLASSCODE(GStringClassCode); class PCL_API LString : public GString { public: static const uint ClassCode() {return LStringClassCode;}; public: LString(const pcont pContainer); virtual void Import(const pcont pContainer, bool AutoRelease = true); virtual const pcont Export(void); public:
 LString(cstr Body = NULL);
 LString(LString &Copy);
 virtual ~LString();

 virtual const VString& operator =(LString& Src);
 virtual const VString& operator =(cstr Src) ;
 virtual const VString& operator +=(cstr Src) ;
 virtual const VString& operator ^=(cstr Src) ;

protected:

 inline uint InitialLen() {return 80;};
};
# 35 "PCL_DString.cpp" 2
#ifdef PCL_TRACEINCLUDES
#	warning "PCL_String.hxx included"
#endif
#endif // __PCL_STRING_H__
