#ifndef PCL_WINFORLIN_H
#define PCL_WINFORLIN_H

#define WINAPI inline
typedef pvoid HINSTANCE, HMODULE;
typedef unsigned int HRESULT, HANDLE, DWORD, HWND, UINT;
typedef int BOOL;
#define TRUE (1)
#define FALSE (0)
#define INFINITE            0xFFFFFFFF  // Infinite timeout
#define MAX_PATH          260

#define E_FAIL        (HRESULT)0x80004005L
#define E_INVALIDARG  (HRESULT)0x80070057L
#define E_OUTOFMEMORY (HRESULT)0x8007000EL
#define E_NOTIMPL     (HRESULT)0x80004001L
#define E_UNEXPECTED  (HRESULT)0x8000FFFFL

#define wcslen(X) (strlen((pchar)X) >> 1)
void * memcpy(void * _Dst, const void * _Src, size_t _Size);
void * memset(void * _Dst, int _Val, size_t _Size);
size_t strlen(const char * _Str);

#include <linux/futex.h>
//#include <linux/asm/atomic.h>
typedef struct _WINFORLIN_CRITICAL_SECTION {
	int /*atomic_t*/ futexState;
} CRITICAL_SECTION;

typedef struct _WINFORLIN_MSG {
    HWND       hwnd;
    UINT        message;
    DWORD      wParam;
    int      lParam;
    double       time;
} MSG, *LPMSG;
#define WAIT_OBJECT_0 (0L)
#define WAIT_TIMEOUT (258L)
BOOL GetMessage(LPMSG lpMsg, HWND hWnd, UINT wMsgFilterMin, UINT wMsgFilterMax);
BOOL TranslateMessage(LPMSG lpMsg);
BOOL DispatchMessage(LPMSG lpMsg);
DWORD MsgWaitForMultipleObjects(
    DWORD nCount,
    const HANDLE *pHandles,
    BOOL fWaitAll,
    DWORD dwMilliseconds,
    DWORD dwWakeMask);
DWORD GetTickCount(void);
HMODULE GetModuleHandle(cstr *lpModuleName);
DWORD GetCurrentDirectory(DWORD nBufferLength, pchar lpBuffer);
#define MB_OK                       0x00000000L
#define MB_ICONHAND                 0x00000010L
#define MB_ICONSTOP                 MB_ICONHAND
int MessageBox(HWND hWnd, cstr lpText, cstr lpCaption, UINT uType);
void ExitProcess(UINT uExitCode);
pchar CharUpper(pchar lpsz);
pchar CharLower(pchar lpsz);
#ifdef PCL_TRACEINCLUDES
#	warning "PCL_WinForLin.h included"
#endif
#endif // PCL_WINFORLIN_H
